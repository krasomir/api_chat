<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;
use App\WebSocket\Chat;

class ChatStart extends Command
{
    protected $signature = 'chat:start';

    protected $description = 'Starts chat server listener';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $server = IoServer::factory( new HttpServer( new WsServer( new Chat() ) ), 51234 );
        $this->info(date('[Y-m-d H:i:s] ') . 'Chat server start.');
        $server->run();
    }
}
